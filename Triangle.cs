﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace gk_lab2
{
    class Triangle : INotifyPropertyChanged
    {
        private List<Vertex> vertices;
        private List<Edge> edges;
        private Edge selectedEdge;
        private Vertex selectedVertex;
        private bool allSelected;

        public event PropertyChangedEventHandler PropertyChanged;
        private ImageBrush imageBrush;
        public ImageBrush ImageBrush
        {
            get
            {
                return imageBrush;
            }
            set
            {
                imageBrush = value;
                OnPropertyChanged("ImageBrush");
            }
        }
        private bool active;
        public bool Active
        {
            get
            {
                return active;
            }
            set
            {
                active = value;
                OnPropertyChanged("Active");
            }
        }
        private bool colorOrTexture;
        public bool ColorOrTexture
        {
            get
            {
                return colorOrTexture;
            }
            set
            {
                colorOrTexture = value;
                OnPropertyChanged("ColorOrTexture");
            }
        }
        private WriteableBitmap texture;
        public WriteableBitmap Texture
        {
            get
            {
                return texture;
            }
            set
            {
                texture = value;
                OnPropertyChanged("Texture");
            }
        }
        public Color Color { get; set; }
        private void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }
        public IEnumerable GetEdgesWithSelection()
        {
            foreach(Edge edge in edges)
            {
                yield return (edge==selectedEdge?true:false||allSelected,edge);
            }
        }
        public IEnumerable GetEdges()
        {
            foreach (Edge edge in edges)
            {
                yield return edge;
            }
        }
        public IEnumerable GetVertices()
        {
            foreach(Vertex vertex in vertices)
            {
                bool isSelected = false;
                if (vertex == selectedVertex) isSelected = true;
                if (selectedEdge != null && (selectedEdge.V1 == vertex || selectedEdge.V2 == vertex)) isSelected = true;
                if (allSelected) isSelected = true;
                yield return (isSelected,vertex);
            }
        }
        public void Select(int x,int y)
        {
            selectedVertex = null;
            selectedEdge = null;
            allSelected = false;
            foreach(Vertex vertex in vertices)
            {
                if(vertex.CloseToVertex(x,y))
                {
                    selectedVertex = vertex;
                    return;
                }
            }
            foreach(Edge edge in edges)
            {
                if(edge.CloseToEdge(x,y))
                {
                    selectedEdge = edge;
                    return;
                }
            }
            allSelected = PointInsidePolygon(x, y);
        }
        public void MoveSelectedItem( Vector vector)
        {
            if(selectedVertex!=null)
            {
                selectedVertex += vector;
                return;
            }
            if(selectedEdge!=null)
            {
                selectedEdge.V1 += vector;
                selectedEdge.V2 += vector;
            }
            if(allSelected)
            {
                for(int i=0;i<vertices.Count;i++)
                    vertices[i]+= vector;
            }
        }
        public int GetMinX()
        {
            int min = int.MaxValue;
            foreach(Vertex vertex in vertices)
            {
                if (vertex.X < min) min = vertex.X;
            }
            return min;
        }
        public Triangle(Vertex v1,Vertex v2, Vertex v3)
        {
            vertices = new List<Vertex>();
            edges = new List<Edge>();
            vertices.Add(v1);
            vertices.Add(v2);
            vertices.Add(v3);
            edges.Add(new Edge(v1, v2));
            edges.Add(new Edge(v2, v3));
            edges.Add(new Edge(v3,v1));
            Color = Color.FromRgb(200,200, 200);
            Texture = new WriteableBitmap(new BitmapImage(new Uri("vertical_icon.png",UriKind.Relative)));
            ImageBrush = new ImageBrush(Texture);
            ColorOrTexture = true;

        }
        public void LoadTexture(string path)
        {
            WriteableBitmap tmp = new WriteableBitmap(new BitmapImage(new Uri(path, UriKind.Absolute)));
            if(tmp.Format!=PixelFormats.Bgr32)
            {
                Texture = new WriteableBitmap( new FormatConvertedBitmap(tmp, PixelFormats.Bgr32, null, 0));
            }
            else
            {
                Texture = tmp;
            }
            ImageBrush = new ImageBrush(Texture);
        }
        public bool PointInsidePolygon(int x, int y)
        {
            Vertex vertex1 = new Vertex(x, y);
            Vertex vertex2 = new Vertex(9999, 9999);
            Edge edge = new Edge(vertex1, vertex2);
            int count = 0;
            foreach (Edge e in edges)
            {
                if (Edge.CrosingCheck(edge, e)) count++;
            }
            if (count % 2 == 1) return true;
            else return false;
        }
    }
}

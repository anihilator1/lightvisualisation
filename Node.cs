﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gk_lab2
{
    class Node :IComparable<Node>
    {
        private int ymax;
        private double x;
        private double a;

        public Node(Edge edge)
        {
            ymax = edge.GetMaxY();
            x = edge.GetMinX();
            a = edge.GetXIncrement();
        }
     
        public int CompareTo(Node other)
        {
            if (x > other.x) return 1;
            if (x <other.x) return -1;
            return 0;
        }

        public int GetX()
        {
            return (int)Math.Round(x);
        }
        public bool IsStillValid(int index)
        {
            if (index >= ymax) return false;
            x += a;
            return true;
        }
    }
}

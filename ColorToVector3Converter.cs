﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media;

namespace gk_lab2
{
    class ColorToVector3Converter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Vector3 vector3 = (Vector3)value;
            return Color.FromRgb((byte)Math.Round(vector3.X*255),(byte) Math.Round(vector3.Y*255), (byte)Math.Round(vector3.Z*255));
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Color color = (Color)value;
            return new Vector3(((float)color.R) / 255, ((float)color.G) / 255, ((float)color.B) / 255);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Drawing;
using System.Windows;

namespace gk_lab2
{
    class DrawingMenager : INotifyPropertyChanged
    {
        private WriteableBitmap myImage;
        private Color drawingColor = Color.FromRgb(0, 0, 0);
        private Color selectionColor = Color.FromRgb(255, 0, 0);
        private Color selecectionColor = Color.FromRgb(0, 0, 255);
        private LightMaster lightMaster;
        public WriteableBitmap MyImage
        {
            get
            {
                return myImage;
            }
            set
            {
                myImage = value;
                OnPropertyChanged("MyImage");

            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }
        private Image image;
        public DrawingMenager(Image image,LightMaster lightMaster)
        {
            this.image = image;
            MyImage = new WriteableBitmap(1000, 610, 96, 96, PixelFormats.Bgr32, null);
            this.lightMaster = lightMaster;
            lightMaster.SetImage(MyImage);
        }
        public void DrawCurrentState(Triangle triangle1, Triangle triangle2)
        {
            FillWithColor(Color.FromRgb(255, 255, 255));
            FillTriangleWithColor(triangle1);
            if (triangle1.Active)
                DrawTriangle(triangle1);
            FillTriangleWithColor(triangle2);
            if (triangle2.Active)
                DrawTriangle(triangle2);
        }
        public void DrawTriangle(Triangle triangle)
        {

            foreach ((bool, Edge) edge in triangle.GetEdgesWithSelection())
            {
                DrawEdge(edge.Item2, edge.Item1 ? selectionColor : drawingColor);
            }
            foreach ((bool, Vertex) vertex in triangle.GetVertices())
            {
                DrawVertex(vertex.Item2, vertex.Item1 ? selectionColor : drawingColor);
            }
        }
        private void FillWithColor(Color color)
        {
            int color_data = color.R << 16;
            color_data|= color.G << 8;
            color_data |=color.B;
            try
            {
                myImage.Lock();
                unsafe
                {
                    int pointer = (int)myImage.BackBuffer;

                    

                    for (int i = 0; i < myImage.PixelHeight; i++)
                    {
                        for (int j = 0; j < myImage.PixelWidth; j++)
                        {
                            *((int*)pointer) = color_data;//lightMaster.ComputeLight(color, i, j);
                            pointer += 4;
                        }
                    }
                }
                myImage.AddDirtyRect(new System.Windows.Int32Rect(0, 0, myImage.PixelWidth, myImage.PixelHeight));
            }
            finally
            {
                myImage.Unlock();
            }
        }
        private void DrawVertex(Vertex vertex, Color color)
        {
            int color_data = color.R << 16;
            color_data |= color.G << 8;
            color_data |= color.B << 0;
            try
            {
                myImage.Lock();
                unsafe
                {
                    int pointer = (int)myImage.BackBuffer;
                    int sy = vertex.Y - 5;
                    int sx = vertex.X - 5;
                    pointer += sy * myImage.BackBufferStride;
                    pointer += sx * 4;
                    for (int i = 0; i < 10; i++)
                    {
                        for (int j = 0; j < 10; j++)
                        {
                            if (sy + i > 0 && sy + i < myImage.PixelHeight && sx + j > 0 && sx + j < myImage.PixelWidth)
                                *((int*)(pointer + j * 4)) = color_data;
                        }
                        pointer += myImage.BackBufferStride;
                    }
                }
            }
            finally
            {
                myImage.Unlock();
            }
        }
        private void DrawEdge(Edge edge, Color color)
        {
            Brezenham(edge.V1, edge.V2, color);
        }
        private void Brezenham(Vertex p1, Vertex p2, Color color)
        {
            int width = myImage.PixelWidth;
            int height = myImage.PixelHeight;

            int color_data = color.R << 16;
            color_data |= color.G << 8;
            color_data |= color.B << 0;

            if (p1.X == p2.X && p1.X >= 0 && p1.X < width)
            {
                int s = Math.Max(Math.Min(p1.Y, p2.Y), 0);
                int sp = s;
                int l = Math.Min(Math.Max(p1.Y, p2.Y), height);
                try
                {
                    myImage.Lock();
                    unsafe
                    {
                        int pointer = (int)myImage.BackBuffer;
                        pointer += s * myImage.BackBufferStride;
                        pointer += p1.X * 4;

                        while (s != l)
                        {
                            if (s >= 0 && s < height)
                            {
                                *((int*)pointer) = color_data;
                            }
                            pointer += myImage.BackBufferStride;
                            s++;
                        }
                    }
                    myImage.AddDirtyRect(new System.Windows.Int32Rect(p1.X, sp, 1, s - sp));
                }
                finally
                {
                    myImage.Unlock();
                }
                return;
            }
            if (p1.Y == p2.Y && p1.Y >= 0 && p1.Y < height)
            {
                int s = Math.Max(Math.Min(p1.X, p2.X), 0);
                int sp = s;
                int l = Math.Min(Math.Max(p1.X, p2.X), width);
                try
                {
                    myImage.Lock();
                    unsafe
                    {
                        int pointer = (int)myImage.BackBuffer;
                        pointer += p1.Y * myImage.BackBufferStride;
                        pointer += s * 4;

                        while (s != l)
                        {
                            if (s >= 0 && s < width)
                            {
                                *((int*)pointer) = color_data;
                            }
                            pointer += 4;
                            s++;
                        }
                    }
                    myImage.AddDirtyRect(new System.Windows.Int32Rect(sp, p1.Y, s - sp, 1));
                }
                finally
                {
                    myImage.Unlock();
                }
                return;
            }

            int x1 = p1.X;
            int px1 = x1;
            int x2 = p2.X;
            int y1 = p1.Y;
            int py1 = y1;
            int y2 = p2.Y;
            int kx = x1 < x2 ? 1 : -1;
            int ky = y1 < y2 ? 1 : -1;
            int dx = Math.Abs(x2 - x1);
            int dy = Math.Abs(y2 - y1);

            try
            {
                myImage.Lock();
                unsafe
                {
                    int pointer = (int)myImage.BackBuffer;
                    pointer += y1 * myImage.BackBufferStride;
                    pointer += x1 * 4;

                    if (x1 >= 0 && x1 < width && y1 >= 0 && y1 < height)
                        *((int*)pointer) = color_data;

                    if (dx >= dy)
                    {
                        int e = dx / 2;
                        while (x1 != x2)
                        {
                            x1 += kx;
                            pointer += kx * 4;
                            e -= dy;
                            if (e < 0)
                            {
                                y1 += ky;
                                pointer += ky * myImage.BackBufferStride;
                                e += dx;
                            }
                            if (x1 >= 0 && x1 < width && y1 >= 0 && y1 < height)
                            {
                                *((int*)pointer) = color_data;
                            }
                        }
                    }
                    else
                    {
                        int e = dy / 2;
                        while (y1 != y2)
                        {
                            y1 += ky;
                            pointer += ky * myImage.BackBufferStride;
                            e -= dx;
                            if (e < 0)
                            {
                                x1 += kx;
                                pointer += kx * 4;
                                e += dy;
                            }
                            if (x1 >= 0 && x1 < width && y1 >= 0 && y1 < height)
                            {
                                *((int*)pointer) = color_data;
                            }

                        }
                    }
                }
                int rx = Math.Min(Math.Max(Math.Min(px1, x2), 0), myImage.PixelHeight);
                int ry = Math.Min(Math.Max(Math.Min(py1, y2), 0), myImage.PixelHeight);

                myImage.AddDirtyRect(new System.Windows.Int32Rect(rx, ry, Math.Min(dx, myImage.PixelWidth - rx), Math.Min(dy, myImage.PixelHeight - ry)));
            }
            finally
            {
                myImage.Unlock();
            }
        }

        public void FillTriangelWithTexture(Triangle triangle)
        {
           
        }
        public void FillTriangleWithColor(Triangle triangle)
        {
            Color color = triangle.Color;
            (List<Node>[] ET, int index) = GenerateET(triangle);
            List<Node> AET = new List<Node>();
            int line = 0;
            int minX = triangle.GetMinX();
            do
            {
                if (ET[index] != null)
                {
                    AET.AddRange(ET[index]);
                }
                AET.Sort();
                if(triangle.ColorOrTexture)
                {
                    FillLine(index, AET, color);
                }
                else
                {
                    FillLine(index, AET, triangle.Texture, line,minX);
                    line++;
                }
                index++;
                AET.RemoveAll(n => !n.IsStillValid(index));
            } while (AET.Count != 0);

        }
        private void FillLine(int lineIndex, List<Node> AET, WriteableBitmap texture, int line,int minX)
        {
            if (AET.Count >= 2)
            {
                try
                {
                    myImage.Lock();
                    unsafe
                    {
                        int texturePointer = (int)texture.BackBuffer;
                        texturePointer += (line % texture.PixelHeight) * texture.BackBufferStride;

                        int pointer = (int)myImage.BackBuffer;
                        pointer += lineIndex * myImage.BackBufferStride;
                        pointer += 4 * AET[0].GetX();
                        int j = AET[0].GetX()-minX;
                        for (int i = AET[0].GetX(); i < Math.Min(AET[1].GetX(), myImage.PixelWidth); i++)
                        {
                            *((int*)(pointer)) = lightMaster.ComputeLight( *((int*)((j % texture.PixelWidth) * 4 + texturePointer)),lineIndex,i);
                            j++;
                            pointer += 4;
                        }
                    }
                }
                finally
                {
                    myImage.Unlock();
                }
            }
        }
        private void FillLine(int lineIndex, List<Node> AET, Color color)
        {
            if (AET.Count >= 2)
            {
                try
                {
                    myImage.Lock();
                    unsafe
                    {
                        int pointer = (int)myImage.BackBuffer;
                        pointer += lineIndex * myImage.BackBufferStride;
                        pointer += 4 * AET[0].GetX();
                        for (int i = AET[0].GetX(); i < Math.Min(AET[1].GetX(), myImage.PixelWidth); i++)
                        {
                            *((int*)(pointer)) = lightMaster.ComputeLight(color,lineIndex,i);
                            *((int*)(pointer)) = lightMaster.ComputeLight(color,lineIndex,i);
                            pointer += 4;
                        }
                    }
                }
                finally
                {
                    myImage.Unlock();
                }
            }


        }
        private (List<Node>[], int) GenerateET(Triangle triangle)
        {
            List<Node>[] ET = new List<Node>[myImage.PixelHeight];
            int min = int.MaxValue;
            foreach (Edge edge in triangle.GetEdges())
            {
                int index = edge.GetMinY();
                if (index < min) min = index;
                if (ET[index] == null)
                {
                    ET[index] = new List<Node>();
                }
                ET[index].Add(new Node(edge));
            }
            return (ET, min);
        }
    }
}

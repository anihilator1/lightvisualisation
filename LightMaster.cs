﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace gk_lab2
{
    class LightMaster : INotifyPropertyChanged

    {

        private WriteableBitmap myImage;
        private Vector3[,] normalVectorMap;
        private (double dhx, double dhy)[,] heightDiferenceMap;
        private Vector3[,] resultMap;
        private Vector3 il;
        private Vector3 lightPoint;
        public Vector3 Il
        {
            get
            {
                return il;
            }
            set
            {
                il = value;
                OnPropertyChanged("Il");
            }
        }
        private bool constL;
        public bool ConstL
        {
            get
            {
                return constL;
            }
            set
            {
                constL = value;
                OnPropertyChanged("ConstL");
            }
        }

        Vector3 D = new Vector3(0, 0, 0);
        private WriteableBitmap normalMap;
        private WriteableBitmap heightMap;
        public ImageBrush ImageBrush
        {
            get
            {
                return new ImageBrush(heightMap);
            }
        }
        public WriteableBitmap HeightMap
        {
            get
            {
                return heightMap;
            }
            set
            {
                heightMap = value;
                OnPropertyChanged("HeightMap");
            }
        }
        public ImageBrush NormalMapBrush
        {
            get
            {
                return new ImageBrush(normalMap);
            }
        }
        private bool normalVectorFromMap;
        public bool NormalVectorFromMap
        {
            get
            {
                return normalVectorFromMap;
            }
            set
            {

                normalVectorFromMap = value;
                ComputeResultMap();
                OnPropertyChanged("NormalVectorFromMap");
            }
        }
        private bool noDisturbance;
        public bool Disturbance
        {
            get
            {
                return noDisturbance;
            }
            set
            {
                noDisturbance = value;
                ComputeResultMap();
                OnPropertyChanged("Disturbance");
            }
        }
        public LightMaster()
        {
            normalMap = new WriteableBitmap(new BitmapImage(new Uri("normal_map.jpg", UriKind.Relative)));
            heightMap = new WriteableBitmap(new BitmapImage(new Uri("brick_heightmap.png", UriKind.Relative)));
            if (heightMap.Format != PixelFormats.Gray8)
            {
                heightMap = new WriteableBitmap(new FormatConvertedBitmap(heightMap, PixelFormats.Gray8, null, 0));
            }
            NormalVectorFromMap = true;
            Disturbance = true;
            constL = true;
            Il = new Vector3(1, 1, 1);
        }
        public void LoadNormalMap(string path)
        {
            WriteableBitmap tmp = new WriteableBitmap(new BitmapImage(new Uri(path, UriKind.Absolute)));
            if (tmp.Format != PixelFormats.Bgr32)
            {
                normalMap = new WriteableBitmap(new FormatConvertedBitmap(tmp, PixelFormats.Bgr32, null, 0));
            }
            else
            {
                normalMap = tmp;
            }
            OnPropertyChanged("NormalMapBrush");
            ComputeNormalVectorMap();
        }
        private void ComputeNormalVectorMap()
        {
            normalVectorMap = new Vector3[myImage.PixelHeight, myImage.PixelWidth];
            unsafe
            {
                int pointer = (int)normalMap.BackBuffer;
                for (int i = 0; i < myImage.PixelHeight; i++)
                {
                    for (int j = 0; j < myImage.PixelWidth; j++)
                    {
                        int tmp = *((int*)(pointer + ((i % normalMap.PixelHeight) * normalMap.BackBufferStride) + ((j % normalMap.PixelWidth) * 4)));
                        normalVectorMap[i, j] = ComputeNormalVector(tmp);
                    }
                }
            }
        }
        private Vector3 ComputeNormalVector(int color)
        {
            byte[] bytes = BitConverter.GetBytes(color);
            Vector3 vector3 = new Vector3((((float)bytes[2]) - 127) / 128, (((float)bytes[1]) - 127) / 128, (((float)bytes[0])) / 255);
            vector3 /= vector3.Z;
            return vector3;
        }
        private void ComputeResultMap()
        {
            if (myImage == null) return;
            resultMap = new Vector3[myImage.PixelHeight, myImage.PixelWidth];
            for(int i=0;i<myImage.PixelHeight;i++)
            {
                for(int j=0;j<myImage.PixelWidth;j++)
                {
                    
                    Vector3 N = !NormalVectorFromMap ? normalVectorMap[i, j] : new Vector3(0, 0, 1);
                    if(Disturbance&&heightDiferenceMap!=null)
                    {
                        float dhx = (float)heightDiferenceMap[i, j].dhx;
                        float dhy = (float)heightDiferenceMap[i, j].dhy;
                        N += new Vector3(dhx, dhy, -N.X * dhx - N.Y * dhy);
                    }
                    resultMap[i,j] = Vector3.Normalize(N);
                }
            }
            
        }
        public void LoadHeightMap(string path)
        {
            WriteableBitmap tmp = new WriteableBitmap(new BitmapImage(new Uri(path, UriKind.Absolute)));
            if (tmp.Format != PixelFormats.Bgr32)
            {
                normalMap = new WriteableBitmap(new FormatConvertedBitmap(tmp, PixelFormats.Gray8, null, 0));
            }
            else
            {
                normalMap = tmp;
            }
            OnPropertyChanged("ImageBrush");
            ComputeDisturbanceMap();
        }
        private void ComputeDisturbanceMap()
        {
            heightDiferenceMap = new (double dhx, double dhy)[myImage.PixelHeight, myImage.PixelWidth];
            unsafe
            {
                int pointer = (int)heightMap.BackBuffer;
                for (int i = 0; i < myImage.PixelHeight; i++)
                {
                    for (int j = 0; j < myImage.PixelWidth; j++)
                    {
                        byte p = *((byte*)(pointer + ((i % heightMap.PixelHeight) * heightMap.BackBufferStride) + ((j % heightMap.PixelWidth))));
                        byte x1 = *((byte*)(pointer + ((i % heightMap.PixelHeight) * heightMap.BackBufferStride) + (((j + 1) % heightMap.PixelWidth))));
                        byte y1 = *((byte*)(pointer + (((i + 1) % heightMap.PixelHeight) * heightMap.BackBufferStride) + ((j % heightMap.PixelWidth))));
                        double dhx =x1 - p;
                        double dhy = y1 - p;
                        heightDiferenceMap[i, j] = (dhx/128, dhy/128);
                    }
                }
            }
        }
        //private double ColorToGray(int color)
        //{
        //    byte[] bytes = BitConverter.GetBytes(color);
        //    return 0.299 * bytes[2] + 0.587 * bytes[1] + 0.144 * bytes[0];
        //}


        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }
        public int ComputeLight(Color color, int x, int y)
        {
            Vector3 Io = new Vector3(color.R, color.G, color.B);
            Vector3 I = Io * Il * ComputeCos(x, y);
            int color_data = ((int)Math.Round(I.X)) << 16;
            color_data |= ((int)Math.Round(I.Y)) << 8;
            color_data |= ((int)Math.Round(I.Z)) << 0;
            return color_data;
        }
        public int ComputeLight(int color, int x, int y)
        {
            byte[] bytes = BitConverter.GetBytes(color);
            Color tmp = Color.FromRgb(bytes[2], bytes[1], bytes[0]);
            return ComputeLight(tmp, x, y);
        }

        float ComputeCos(int x, int y)
        {
            Vector3 N;
            //if (normalVectorFromMap)
            //{
            //    N = new Vector3(0, 0, 1);
            //}
            //else
            //{
            //    N = normalVectorMap[x, y];
            //}
            //if(Disturbance)
            //{
            //    float dhx = (float)heightDiferenceMap[x, y].dhx;
            //    float dhy = (float)heightDiferenceMap[x, y].dhy;
            //    Vector3 D = new Vector3(dhx, dhy, -N.X*dhx-N.Y * dhy);
                
            //    N += D;
            //}
            N = resultMap[x, y];
            //N = Vector3.Normalize(N);
            Vector3 L = ComputeL(x,y);
            float tmp = N.X * L.X + N.Y * L.Y + N.Z * L.Z;
            return tmp < 0 ? 0 : tmp;
        }
        public void SetImage(WriteableBitmap myImage)
        {
            this.myImage = myImage;
            ComputeNormalVectorMap();
            ComputeDisturbanceMap();
            ComputeResultMap();
        }
        public void SetLightPoint(Vector3 v)
        {
            lightPoint = v;
        }
        private Vector3 ComputeL(int x,int y)
        {
            if(constL)
            {
                return new Vector3(0, 0, 1);
            }
            else
            {
                return Vector3.Normalize(lightPoint - new Vector3(x, y, 0));
            }
        }

    }
}

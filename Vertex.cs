﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gk_lab2
{
    class Vertex
    {
        public int  X { get; set; }
        public int Y { get; set; }
        private int tolerance = 20;
        public Vertex(int x, int y)
        {
            this.X = x;
            this.Y = y;
        }
        public bool CloseToVertex(int x1, int y1)
        {
            if (Math.Pow((X - x1), 2) + Math.Pow(Y - y1, 2) < Math.Pow(tolerance, 2)) return true;
            return false;
        }
        public static Vertex operator -(Vertex v1, Vertex v2)
        {
            return new Vertex(v1.X - v2.X, v1.Y - v2.Y);
        }
        public static Vertex operator +(Vertex vertex,Vector vector)
        {
            vertex.X += vector.X;
            vertex.Y += vector.Y;
            return vertex;
        }

    }
}

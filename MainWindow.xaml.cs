﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace gk_lab2
{
    /// <summary>
    /// Logika interakcji dla klasy MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private DrawingMenager drawingMenager;
        private Triangle triangle1;
        private Triangle triangle2;
        private bool moveInProgress;
        private bool justClick;
        private LightMaster lightMaster;
        int oldX;
        int oldY;
        int currentAngle = 0;
        int radius = 100;
        public MainWindow()
        {
            InitializeComponent();
            InitDrawing();
            InitTriangles();
            InitHandlers();
            InitColorMenu();
            InitTimer();
        }
        private void InitTimer()
        {
            DispatcherTimer dispatcherTimer = new System.Windows.Threading.DispatcherTimer();
            dispatcherTimer.Tick += TimerTick;
            dispatcherTimer.Interval = new TimeSpan(0, 0, 0, 0, 1);
            dispatcherTimer.Start();
        }

        private void TimerTick(object sender, EventArgs eventArgs)
        {
            (double x, double y) = PolarToCartesian(radius,currentAngle);
            lightMaster.SetLightPoint(new Vector3((int)Math.Round(x),(int)Math.Round(y), 50));
            drawingMenager.DrawCurrentState(triangle1, triangle2);
            currentAngle += 10;
            (double, double) PolarToCartesian(Double radius, Double angleDegree)
            {
                double angleRadian = angleDegree * 2 * Math.PI / 360;
                double x1 = radius * Math.Cos(angleRadian);
                double y1 = radius * Math.Sin(angleRadian);
                return (x1+300, y1+500);
            }
        }
        private void InitHandlers()
        {
            drawing_space.MouseMove += OnMouseMove;
            drawing_space.MouseUp += OnMouseUp;
            drawing_space.MouseDown += OnMouseDown;
            color_picker.SelectedColorChanged += Redraw;
            color_RadioButton.Checked += Redraw;
            texture_RadioButton.Checked += Redraw;
            texture_Button.Click += TextureButtonClick;
            normalMap_Button.Click += NormalMapButtonClick;
            lightcolor_picker.SelectedColorChanged += Redraw;
            noDisturbance_RadioButton.Checked += Redraw;
            disturbance_RadioButton.Checked += Redraw;
            normalVectorFromMap_RadioButton.Checked += Redraw;
            constNormalVector_RadioButton.Checked += Redraw;
        }
        private void InitColorMenu()
        {
            color_RadioButton.IsChecked = true;
            color_picker.SelectedColor = Color.FromRgb(0, 0, 0);
            objectColor_GroupBox.DataContext = triangle1;
        }
        private void TextureButtonClick(object sender, EventArgs eventArgs)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Image files (*.png;*.jpeg;*jpg)|*.png;*.jpeg;*jpg";
            if (openFileDialog.ShowDialog() == true)
            {
                if (triangle1.Active) triangle1.LoadTexture(openFileDialog.FileName);
                if (triangle2.Active) triangle2.LoadTexture(openFileDialog.FileName);
            }
        }
        private void NormalMapButtonClick(object sender, EventArgs eventArgs)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Image files (*.png;*.jpeg;*jpg)|*.png;*.jpeg;*jpg";
            if (openFileDialog.ShowDialog() == true)
            {
                lightMaster.LoadNormalMap(openFileDialog.FileName);
            }
        }
        private void Redraw(object sender, EventArgs eventArgs)
        {
            drawingMenager.DrawCurrentState(triangle1, triangle2);
        }
        private void OnMouseDown(object sender, MouseEventArgs mouseEventArgs)
        {
            if (mouseEventArgs.LeftButton == MouseButtonState.Pressed)
            {

                int x = (int)mouseEventArgs.GetPosition(drawing_space).X;
                int y = (int)mouseEventArgs.GetPosition(drawing_space).Y;


                moveInProgress = true;
                justClick = true;
                oldX = (int)mouseEventArgs.GetPosition(drawing_space).X;
                oldY = (int)mouseEventArgs.GetPosition(drawing_space).Y;
            }

        }
        private void OnMouseUp(object sender, MouseEventArgs mouseEventArgs)
        {
            if (mouseEventArgs.LeftButton == MouseButtonState.Released)
            {
                moveInProgress = false;
                if (justClick)
                {
                    int x = (int)mouseEventArgs.GetPosition(drawing_space).X;
                    int y = (int)mouseEventArgs.GetPosition(drawing_space).Y;
                    triangle1.Active = false;
                    triangle2.Active = false;
                    if (triangle1.PointInsidePolygon(x, y))
                    {
                        triangle1.Active = true;
                        objectColor_GroupBox.DataContext = triangle1;
                        triangle1.Select(x, y);
                    }
                    else if (triangle2.PointInsidePolygon(x, y))
                    {
                        triangle2.Active = true;
                        objectColor_GroupBox.DataContext = triangle2;
                        triangle2.Select(x, y);
                    }
                    drawingMenager.DrawCurrentState(triangle1, triangle2);
                }
            }

        }
        private void OnMouseMove(object sender, MouseEventArgs mouseEventArgs)
        {
            int x = (int)mouseEventArgs.GetPosition(drawing_space).X;
            int y = (int)mouseEventArgs.GetPosition(drawing_space).Y;
            if (!moveInProgress)
            {
                if (triangle1.Active)
                    triangle1.Select(x, y);
                if (triangle2.Active)
                    triangle2.Select(x, y);
            }
            else
            {
                if (triangle1.Active)
                    triangle1.MoveSelectedItem(new Vector(x - oldX, y - oldY));
                if (triangle2.Active)
                    triangle2.MoveSelectedItem(new Vector(x - oldX, y - oldY));
                justClick = false;
            }
            drawingMenager.DrawCurrentState(triangle1, triangle2);
            oldX = x;
            oldY = y;
        }
        private void InitDrawing()
        {
            lightMaster = new LightMaster();
            drawingMenager = new DrawingMenager(drawing_space, lightMaster);
            drawing_space.DataContext = drawingMenager;
            this.DataContext = lightMaster;
        }
        private void InitTriangles()
        {
            triangle1 = new Triangle(new Vertex(0, 0), new Vertex(990, 600), new Vertex(990, 0));
            triangle2 = new Triangle(new Vertex(0, 0), new Vertex(990, 600), new Vertex(0, 600));
            drawingMenager.DrawCurrentState(triangle1, triangle2);
        }
    }
}
